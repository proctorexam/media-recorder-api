package bytestream

import (
	"io"

	"github.com/at-wat/ebml-go"
	"github.com/at-wat/ebml-go/webm"
)

type Media interface {
	Encode(io.Writer) error
}

func Decode(r io.Reader) (Media, error) {
	m := new(webmContainer)
	err := ebml.Unmarshal(r, m)
	if err != nil && !IsStreamError(err) {
		return nil, err
	}
	m.Segment.Info.Duration = duration(m)
	return m, nil
}

func Encode(m Media, w io.Writer) error {
	return ebml.Marshal(m, w)
}

type webmContainer struct {
	Header  webm.EBMLHeader    `ebml:"EBML"`
	Segment webm.SegmentStream `ebml:"Segment,size=unknown"`
}

func (wc *webmContainer) Encode(w io.Writer) error {
	return Encode(wc, w)
}

func duration(wc *webmContainer) float64 {
	lc := wc.Segment.Cluster[len(wc.Segment.Cluster)-1]
	fc := wc.Segment.Cluster[0]
	lt := lc.Timecode + uint64(lc.SimpleBlock[len(lc.SimpleBlock)-1].Timecode)
	ft := fc.Timecode + uint64(fc.SimpleBlock[0].Timecode)
	return float64(lt - ft)
}
