package bytestream

import (
	"errors"
	"fmt"
	"io"
)

func Copy(r io.Reader, w io.Writer) error {
	_, err := io.Copy(w, r)
	if err != nil && !IsStreamError(err) {
		return err
	}
	return nil
}

func IsStreamError(err error) bool {
	return errors.As(err, sError)
}

// http2 stream error, to catch canceled streams (refreshed browser page...)
type streamErrCode uint32

type streamError struct {
	StreamID uint32
	Code     streamErrCode
	Cause    error
}

func (e streamError) Error() string {
	return fmt.Sprintf("ID %v, code %v", e.StreamID, e.Code)
}

var sError = &streamError{}
