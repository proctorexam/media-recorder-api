package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"

	"syscall"

	// _ "net/http/pprof"

	"gitlab.com/proctorexam/media-recorder-api/recorder"
)

var (
	addr    = flag.String("addr", ":8080", "http server listen addr")
	tlscert = flag.String("tlscert", "certs/cert.pem", "tls cert file")
	tlskey  = flag.String("tlskey", "certs/key.pem", "tls key file")
)

func main() {
	flag.Parse()

	rec1 := recorder.NewOne(filepath.Join("recordings", "1"))
	http.HandleFunc("/1", corsHandler(recordHandler(rec1)))

	rec2 := recorder.NewTwo(filepath.Join("recordings", "2"))
	http.HandleFunc("/2", corsHandler(recordHandler(rec2)))

	rec3 := recorder.NewThree(filepath.Join("recordings", "3"))
	http.HandleFunc("/3", corsHandler(recordHandler(rec3)))

	rec4 := recorder.NewFour(filepath.Join("recordings", "4"), "eu-west-1", "media-recorder-poc")
	http.HandleFunc("/4", corsHandler(recordHandler(rec4)))

	rec5 := recorder.NewFive(filepath.Join("recordings", "5"), "eu-west-1", "media-recorder-poc")
	http.HandleFunc("/5", corsHandler(recordHandler(rec5)))

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Fatal(http.ListenAndServeTLS(*addr, *tlscert, *tlskey, nil))
	}()

	log.Println("server started")

	// stopCPUProfile := startCPUProfile()
	// defer stopCPUProfile()
	// stopMEMProfile := startMEMProfile()
	// defer stopMEMProfile()

	<-sig

	log.Println("server stopped")

	rec1.Stop()
	rec2.Stop()
	rec3.Stop()
	rec4.Stop()
	rec5.Stop()

}

func corsHandler(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")

		if r.Method == http.MethodOptions || r.Method == http.MethodHead {
			return
		}
		h(w, r)
	}
}

func recordHandler(rec recorder.Recorder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(404)
			return
		}

		name := r.URL.Query().Get("name")
		if name == "" {
			w.WriteHeader(400)
			return
		}

		if err := rec.Record(name, r.Body); err != nil {
			w.WriteHeader(500)
			return
		}
	}
}

// profile

// const goProfilePath = "profile/go"

// func startCPUProfile() func() {
// 	fn := filepath.Join(goProfilePath, "cpu.pprof")
// 	f, err := os.Create(fn)
// 	if err != nil {
// 		log.Fatalf("profile: could not create cpu profile %q: %v", fn, err)
// 	}
// 	log.Printf("profile: cpu profiling enabled, %s\n", fn)
// 	pprof.StartCPUProfile(f)
// 	return func() {
// 		pprof.StopCPUProfile()
// 		f.Close()
// 		log.Printf("profile: cpu profiling disabled, %s\n", fn)
// 	}
// }
// func startMEMProfile() func() {
// 	fn := filepath.Join(goProfilePath, "mem.pprof")
// 	f, err := os.Create(fn)
// 	if err != nil {
// 		log.Fatalf("profile: could not create memory profile %q: %v", fn, err)
// 	}
// 	old := runtime.MemProfileRate
// 	runtime.MemProfileRate = 4096
// 	log.Printf("profile: memory profiling enabled (rate %d), %s\n", runtime.MemProfileRate, fn)
// 	return func() {
// 		pprof.Lookup("allocs").WriteTo(f, 0)
// 		f.Close()
// 		runtime.MemProfileRate = old
// 		log.Printf("profile: memory profiling disabled, %s\n", fn)
// 	}
// }
