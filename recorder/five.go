package recorder

import (
	"context"
	"fmt"
	"io"
	"log"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/proctorexam/media-recorder-api/bytestream"
)

type five struct {
	dir    string
	region string
	bucket string
	up     *manager.Uploader
}

func NewFive(dir, region, bucket string) Recorder {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithSharedConfigProfile("playground"))
	if err != nil {
		log.Fatal(err)
	}
	cfg.Region = region

	up := manager.NewUploader(s3.NewFromConfig(cfg))
	up.LeavePartsOnError = true

	rec := &five{
		dir:    dir,
		bucket: bucket,
		up:     up,
	}
	return rec
}

func (rec *five) Record(name string, r io.Reader) error {
	ts := time.Now().UTC().Unix()

	pr, pw := io.Pipe()
	defer func() {
		pw.Close()
	}()

	go func() {
		err := rec.upload(filepath.Join(rec.dir, name, fmt.Sprintf("%v.webm", ts)), pr)
		if err != nil {
			log.Println("five upload", err)
		}
	}()

	m, err := bytestream.Decode(io.TeeReader(r, pw))
	pw.Close()
	if err != nil {
		return err
	}

	ur, uw := io.Pipe()
	defer func() {
		uw.Close()
	}()

	go func() {
		err := rec.upload(filepath.Join(rec.dir, name, fmt.Sprintf("%v-enc.webm", ts)), ur)
		if err != nil {
			log.Println("five upload env", err)
		}
	}()

	err = m.Encode(uw)
	uw.Close()

	return err
}

func (rec *five) Stop() {
}

func (rec *five) upload(key string, body io.Reader) error {
	_, err := rec.up.Upload(context.TODO(), &s3.PutObjectInput{
		Bucket:      aws.String(rec.bucket),
		Key:         aws.String(key),
		ContentType: aws.String("video/webm"),
		Body:        body,
	})
	return err
}
