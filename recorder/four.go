package recorder

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/proctorexam/media-recorder-api/bytestream"
)

type four struct {
	dir    string
	region string
	bucket string
	up     *manager.Uploader
}

func NewFour(dir, region, bucket string) Recorder {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithSharedConfigProfile("playground"))
	if err != nil {
		log.Fatal(err)
	}
	cfg.Region = region

	up := manager.NewUploader(s3.NewFromConfig(cfg))
	up.LeavePartsOnError = true

	rec := &four{
		dir:    dir,
		bucket: bucket,
		up:     up,
	}
	return rec
}

func (rec *four) Record(name string, r io.Reader) error {
	ts := time.Now().UTC().Unix()

	c, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { c.Close() }()

	m, err := bytestream.Decode(io.TeeReader(r, c))
	if err != nil {
		return err
	}

	w, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v-enc.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { w.Close() }()

	err = m.Encode(w)
	if err != nil {
		return err
	}

	_, err = w.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	err = rec.upload(filepath.Join(rec.dir, name, fmt.Sprintf("%v-enc.webm", ts)), w)
	if err != nil {
		log.Println("four upload err", err)
	}
	return err
}

func (rec *four) Stop() {
}

func (rec *four) upload(key string, body io.Reader) error {
	_, err := rec.up.Upload(context.TODO(), &s3.PutObjectInput{
		Bucket:      aws.String(rec.bucket),
		Key:         aws.String(key),
		ContentType: aws.String("video/webm"),
		Body:        body,
	})
	return err
}
