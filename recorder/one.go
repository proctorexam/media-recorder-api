package recorder

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/proctorexam/media-recorder-api/bytestream"
)

type one struct {
	dir string
}

func NewOne(dir string) Recorder {
	return &one{dir: dir}
}

func (rec *one) Record(name string, r io.Reader) error {
	ts := time.Now().UTC().Unix()
	c, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { c.Close() }()
	err = bytestream.Copy(r, c)
	return err
}

func (rec *one) Stop() {
}
