package recorder

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

type Recorder interface {
	Record(name string, r io.Reader) error
	Stop()
}

func openFile(name string, flag int) (*os.File, error) {
	if err := os.MkdirAll(filepath.Dir(name), os.ModePerm); err != nil {
		return nil, err
	}

	return os.OpenFile(name, flag, os.ModePerm)
}

func newFilename() string {
	return fmt.Sprintf("%v.webm", time.Now().UTC().Unix())
}
