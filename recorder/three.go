package recorder

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/proctorexam/media-recorder-api/bytestream"
)

type three struct {
	dir string
}

func NewThree(dir string) Recorder {
	return &three{dir: dir}
}

func (rec *three) Record(name string, r io.Reader) error {
	ts := time.Now().UTC().Unix()
	c, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { c.Close() }()

	m, err := bytestream.Decode(io.TeeReader(r, c))
	if err != nil {
		return err
	}

	w, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v-enc.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { w.Close() }()

	return m.Encode(w)
}

func (rec *three) Stop() {
}
