package recorder

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/proctorexam/media-recorder-api/bytestream"
)

type two struct {
	dir string
}

func NewTwo(dir string) Recorder {
	return &two{dir: dir}
}

func (rec *two) Record(name string, r io.Reader) error {
	ts := time.Now().UTC().Unix()
	c, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { c.Close() }()
	err = bytestream.Copy(r, c)
	if err != nil {
		return err
	}

	c.Seek(0, io.SeekStart)
	m, err := bytestream.Decode(c)
	if err != nil {
		return err
	}

	w, err := openFile(filepath.Join(rec.dir, name, fmt.Sprintf("%v-enc.webm", ts)), os.O_CREATE|os.O_RDWR)
	if err != nil {
		return err
	}
	defer func() { w.Close() }()

	return m.Encode(w)
}

func (rec *two) Stop() {
}
