#!/usr/bin/env bash

h="mediarecorderapi.ddns.net"
r="4" # 1,2,3,4,5 sets recorder

for i in {1..1000}
do
ffmpeg -hide_banner -loglevel info -f lavfi -i testsrc=d=60:size=640x480:rate=5 -fflags nobuffer -deadline realtime -f webm "https://$h/$r?name=test-1k-$i" &
done